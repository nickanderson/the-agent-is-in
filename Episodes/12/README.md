- [Installing Spacemacs](#org441777c)
- [Download and Install CFEngine](#org2c39dd7)
  - [Install cf-remote](#orgc2942eb)
  - [Install CFEngine](#org89fed39)
    - [Setup unprivledged execution](#org72a838b)
- [Configuring Spacemacs](#orgea69c78)
  - [Layers](#org8a6f631)
  - [Additional Packages](#orgaeb2ddd)
  - [user-config](#org979169b)
  - [Check out the basics](#org28ebbca)
    - [Snippets](#org7c63a21)
    - [Projects](#org1174cfd)
    - [Git](#org0d132b3)

![img](./thumbnail.png)


<a id="org441777c"></a>

# Installing Spacemacs

First we need to install emacs.

```sh
sudo apt -y install emacs
```

Then we need to clone the spacemacs repository into place.

```sh
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
```

Now we need to launch `emacs`. It will ask some basic setup questions, take the defaults. Then it will install some packages, this will take a few minutes. While we wait, lets install CFEngine.


<a id="org2c39dd7"></a>

# Download and Install CFEngine


<a id="orgc2942eb"></a>

## Install cf-remote

`cf-remote` is a small tool to help you find and install various versions of CFEngine. It's written in python and you can get it via `pip`.

First we need to install pip.

```sh
apt -y install python3-pip
```

And it usually wants to be upgraded.

```sh
pip3 install pip --upgrade
```

Now we can install `cf-remote`.

```sh
pip3 install cf-remote
```

This installed the updated version of pip3 into `~/.local/bin`, we want that directory in our path so, source `~/.profile` which already contains logic to add the directory to `$PATH` if it exists.

```sh
source ~/.profile
```


<a id="org89fed39"></a>

## Install CFEngine

Let's download the latest community agent package for Ubuntu 20 and install it with `apt`.

```sh
curl -s --output-dir /tmp/ --remote-name $(cf-remote --version master list --edition community ubuntu20 agent | tail -n 1)
```

<div class="NOTE" id="org2a05825">
<p>
You'll need to be <code>root</code>, use <code>sudo</code> or otherwise elevate privileges.
</p>

</div>

```sh
apt -y install /tmp/cfengine-*
```


<a id="org72a838b"></a>

### Setup unprivledged execution

We want to setup unprivledged execution so that we can benefit from on the fly syntax checking as well as snippet execution from within `org-mode`.

Seed masterfiles into the local users *inputs* and link the local users *bin* dir.

```sh
rsync --mkpath --archive /var/cfengine/share/CoreBase/masterfiles/ ~/.cfagent/inputs/
ln --symbolic --force /var/cfengine/bin ~/.cfagent/bin
```


<a id="orgea69c78"></a>

# Configuring Spacemacs

Type `SPC f e d` to start editing the spacemacs configuration file.


<a id="org8a6f631"></a>

## Layers

Search for `dotspacemacs-configuration-layers` and add/uncomment the following:

-   `auto-completion`
-   `org`
-   `git`
-   `cfengine`
-   `syntax-checking`

Find documentation for each layer by typing `SPC h l` and then the name of the layer.


<a id="orgaeb2ddd"></a>

## Additional Packages

To make things look a bit nicer in `dotspacemacs-additional-package` add `org-modern`.


<a id="org979169b"></a>

## user-config

Configure support for languages you want to be able to execute in `dotspacemacs/user-config`

```elisp
;; Automatically reload file if it changes on disk
(setq global-auto-revert-mode t)

;; Familiar zooming with Ctrl+ and Ctrl-
(define-key global-map (kbd "C-+") 'text-scale-increase) ;; Ctrl Shift +
(define-key global-map (kbd "C--") 'text-scale-decrease) ;; Ctrl -


(with-eval-after-load 'org
  (setq org-hide-emphasis-markers t) ;; 
  (add-hook 'org-mode-hook #'org-modern-mode) ;; Make org-mode look a bit nicer
  (add-hook 'org-agenda-finalize-hook #'org-modern-agenda) ;; Make the agenda look a bit nicer
  (setq org-startup-indented t) ;; Hides inline markup characters =verbatium= /italic/ *bold*
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t) ; Works for sh, shell, bash
     (sql . t) ; PostgreSQL and more https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-sql.html
     (cfengine3 . t) ; CFEngine
     (python . t)
     )))
```

Now, let's re-start again by typing `SPC q r`. Spacemacs will install some more packages and then we will be ready to cehck out the basics.


<a id="org28ebbca"></a>

## Check out the basics

Let's open a CFEngine policy file by running `SPC f f` and then typing `~/.cfagent/inputs/services/main.cf`

Spacemacs will show you completion paths that you can tab complete.

-   Syntax highlighting for `.cf` files
-   Syntax checking
    -   `SPC e l`
-   Function prototypes (eldoc)


<a id="org7c63a21"></a>

### Snippets

-   cfengine3-mode

    Jeff Carlson kindly published an extensive set of snippets, let's get it.
    
    ```sh
    mkdir -p ~/.emacs.d/private/snippets/cfengine3-mode/
    git clone https://github.com/jeffcarlson72/emacs-yasnippet-cfengine3-mode ~/.emacs.d/private/snippets/cfengine3-mode/
    ```

-   org-mode

    I find it super handy to have a CFEngine snippet for use in `org-mode`.
    
    Let's make a directory to snippets for `org-mode`.
    
    ```sh
    mkdir -p ~/.emacs.d/private/snippets/org-mode/
    ```
    
    Next we want to create `~/.emacs.d/private/snippets/org-mode/cf3` with the following content.
    
        # -*- mode: snippet -*-
        # name : cf3-snippet
        # key : <cf3
        # contributor : Nick Anderson <nick@cmdln.org>
        # group : cfengine3
        # expand-env: ((yas-indent-line 'fixed) (yas-wrap-around-region 'nil))
        # --
        #+caption: Example Policy
        #+begin_src cfengine3 :include-stdlib t :log-level info :exports both
        bundle agent __main__
        # @brief Drive default bundlesequence if called as policy entry
        {
          methods:
              "$2"
                usebundle => $1();
        }
        bundle agent ${1:BundleName}()
        # @brief $1 ${2:is responsible for ...}
        {
          $0
        }
        #+end_src
    
    Then run `yas-reload-all`
    
    -   Check it out &#x2026;
    
        Type `SPC i s` and start typing `cf3-snippet`.
        
        Execute the block with `org-babel-execute-maybe` (`, b e`).


<a id="org1174cfd"></a>

### Projects

Spacemacs uses projectile for project management.

`.git` directories automatically register a project, but you can make any directory a project root by creating a `.projectile` file.

```sh
touch ~/.cfagent/inputs/.projectile 
```

Let's open main.cf that we had previously opened. Try `SPC b b` to start searching for open buffers and type `main.cf`.

Now, look for other files in the project we are currently in with `SPC p f`.

There are other cool things too

-   Project wide search and replace
-   Project *build* and *compile* shortcuts

-   Custom project types

    You can define *custom* project types, perhaps one for *masterfiles* ..
    
    ```emacs-lisp
    (projectile-register-project-type 'masterfiles '("package.json")
                                      :project-file "promises.cf"
              :test "cf-promises -cf ./update.cf; cf-promises -cf ./promises.cf"
              :run "cf-agent -KIf ./promises.cf")
    ```


<a id="org0d132b3"></a>

### Git

-   Cloning a repository

    Run `SPC g c` and answer the questions.
    
    e.g. <https://github.com/nickanderson/cfengine-lynis>

-   Working with Git Forges

    Open, comment, assign reviewers, merge etc &#x2026; Pull/Merge requests with GitHub/GitLab or other supported forges.
